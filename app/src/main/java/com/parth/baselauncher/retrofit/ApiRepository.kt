package com.parth.baselauncher.retrofit



class ApiRepository(private val apiService: ApiService) {

    suspend fun getUsers() = apiService.getUsers()

}
