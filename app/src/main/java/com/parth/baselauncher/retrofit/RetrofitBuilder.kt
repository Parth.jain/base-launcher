package com.parth.baselauncher.retrofit

import android.util.Log
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitBuilder {

    private val BASE_URL = "https://5e510330f2c0d300147c034c.mockapi.io/"
    private var logging = HttpLoggingInterceptor(object : HttpLoggingInterceptor.Logger {
        override fun log(message: String) {
            Log.d("api message log", message)
        }
    })
    private val httpClient = OkHttpClient.Builder()

    init {
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.connectTimeout(1, TimeUnit.MINUTES)
        httpClient.addInterceptor(logging)
    }

    private fun getRetrofit(): Retrofit {
        val jsonBuilder = GsonBuilder()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(httpClient.build())
            .addConverterFactory(GsonConverterFactory.create(jsonBuilder.setLenient().create()))
            .build()
    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}

/*

object RetrofitBuilder {

    private const val BASE_URL = "https://5e510330f2c0d300147c034c.mockapi.io/"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build() //Doesn't require the adapter
    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}*/
