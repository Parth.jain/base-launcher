package com.parth.baselauncher.view

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.res.Configuration
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.parth.baselauncher.R
import com.parth.baselauncher.adapters.AppListAdapter
import com.parth.baselauncher.extensions.openActivity
import com.parth.baselauncher.models.AppData
import com.parth.baselauncher.viewmodels.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {
    lateinit var mainActivityViewModel: MainActivityViewModel
    lateinit var applicationList:List<AppData>
    var flagDecoration = false

    override fun getLayoutId() =R.layout.activity_main

    override fun setupObservers() {
        mainActivityViewModel.devicesApplicationList.observe(this, {
            setDockAppForFirst(it)
            setAppListInRv(it)
        })
    }

    override fun setupViewModel() {
        mainActivityViewModel=ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        mainActivityViewModel.fetchApplicationListFromDevice(packageManager, this.packageName)
        val filter = IntentFilter()
        filter.addAction(Intent.ACTION_PACKAGE_ADDED)
        filter.addAction(Intent.ACTION_PACKAGE_REMOVED)
        filter.addAction(Intent.ACTION_PACKAGE_CHANGED)
        filter.addDataScheme("package")
        registerReceiver(AppInstallerReceiver(), filter)
    }

    lateinit var gridLayoutManager:GridLayoutManager
    private fun setAppListInRv(appList: ArrayList<AppData>) {
        //remove app-list contained in system Tray area
        appList.removeAll(appsInSystemTray)
        val orientation = resources.configuration.orientation
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
             gridLayoutManager =
                GridLayoutManager(applicationContext, 3,LinearLayoutManager.HORIZONTAL, false)
        } else {
            // In portrait
             gridLayoutManager =
                GridLayoutManager(applicationContext,4, LinearLayoutManager.HORIZONTAL, false)
        }

        //For vertical grid
      /*  val gridLayoutManager =
            GridLayoutManager(applicationContext, 4, LinearLayoutManager.VERTICAL, false)*/
        appListRv.layoutManager = gridLayoutManager
        appListRv.setHasFixedSize(true)

        if (!flagDecoration) {
            appListRv.addItemDecoration(ItemOffsetDecoration(this, R.dimen.item_offset))
            flagDecoration = true
        }
        val appList2=appList.sortedWith(
            Comparator<AppData> { o1, o2 ->
                o1?.appName?.compareTo(o2?.appName ?: "", true) ?: 0
            }
        )
        applicationList= appList2
        appListRv.adapter = AppListAdapter(this).also {
            it.passAppList(appList2)
        }
    }

    //Defined list of system apps might be change once get other list
    private val packageDock = arrayOf(
        "com.android.dialer",
        "com.google.android.dialer",
        "com.android.messaging",
        "com.google.android.apps.messaging",
        "com.android.chrome",
        "com.android.browser",
        "com.google.android.music",
        "com.android.music",
        "com.android.contacts",
        "com.google.android.apps.photos",
        "com.android.settings",
        "com.android.camera2",
        "com.google.android.apps.docs",
        "com.android.documentsui",
        "com.google.android.videos",
        "com.google.android.apps.maps",
        "com.google.android.apps.photos",
        "org.chromium.webview_shell",
        "com.google.android.youtube",
        "com.google.android.googlequicksearchbox"
    )

    private val appsInSystemTray: ArrayList<AppData> = ArrayList<AppData>()
    private fun setDockAppForFirst(appList: ArrayList<AppData>) {
        var app: AppData?
        for (packageName in packageDock) {
            app = findApp(appList, packageName)
            if (app != null) {
                if (appsInSystemTray.size < 4) {
                    appsInSystemTray.add(app)
                } else {
                    break
                }
            }
        }

        if(appsInSystemTray.size>0) {
            loadDockApp(appsInSystemTray)
        }
    }

    private fun loadDockApp(appsInSystemTray: ArrayList<AppData>) {
        systemTrayApp1.setImageDrawable(appsInSystemTray[0].icon)
        systemTrayApp2.setImageDrawable(appsInSystemTray[1].icon)
        systemTrayApp3.setImageDrawable(appsInSystemTray[2].icon)
        systemTrayApp4.setImageDrawable(appsInSystemTray[3].icon)

        systemTrayApp1.setOnClickListener {
            it.openActivity(this, appsInSystemTray[0].packageName)
        }
        systemTrayApp2.setOnClickListener {
            it.openActivity(this, appsInSystemTray[1].packageName)
        }
        systemTrayApp3.setOnClickListener {
            it.openActivity(this, appsInSystemTray[2].packageName)
        }
        systemTrayApp4.setOnClickListener {
            it.openActivity(this, appsInSystemTray[3].packageName)
        }
    }

    private fun findApp(appsList: List<AppData>, packageName: String?): AppData? {
        var app: AppData? = null
        for (tempApp in appsList) {
            if (tempApp.packageName == packageName) {
                app = tempApp
                break
            }
        }
        return app
    }

    inner class AppInstallerReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            mainActivityViewModel.fetchApplicationListFromDevice(
                packageManager,
                (context!! as MainActivity).packageName
            )
        }
    }

    override fun onBackPressed() {
        //super.onBackPressed()
    }
}