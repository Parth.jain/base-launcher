package com.parth.baselauncher.view

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.parth.baselauncher.R
import com.parth.baselauncher.adapters.MainAdapter
import com.parth.baselauncher.extensions.hide
import com.parth.baselauncher.extensions.show
import com.parth.baselauncher.extensions.showToast
import com.parth.baselauncher.utilities.Status
import com.parth.baselauncher.utilities.ViewModelFactory
import com.parth.baselauncher.models.User
import com.parth.baselauncher.retrofit.RetrofitBuilder.apiService
import com.parth.baselauncher.viewmodels.RetrofitSampleViewModel
import kotlinx.android.synthetic.main.activity_retrofit_sample.*

class RetrofitSampleActivity : BaseActivity() {

    private lateinit var viewModel: RetrofitSampleViewModel
    private lateinit var adapter: MainAdapter

    override fun getLayoutId()=R.layout.activity_retrofit_sample

    override fun setupViewModel() {
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(apiService)
        ).get(RetrofitSampleViewModel::class.java)
    }

    override fun setupObservers() {
     /*   viewModel.getUsers().observe(this, Observer {
            it?.let { resource ->
                when (resource.status) {
                    Status.SUCCESS -> {
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        resource.data?.let { users -> retrieveList(users) }
                    }
                    Status.ERROR -> {
                        recyclerView.visibility = View.VISIBLE
                        progressBar.visibility = View.GONE
                        Toast.makeText(this, it.message, Toast.LENGTH_LONG).show()
                    }
                    Status.LOADING -> {
                        progressBar.visibility = View.VISIBLE
                        recyclerView.visibility = View.GONE
                    }
                }
            }
        })*/

        viewModel.userList.observe(this, Observer {
            recyclerView.visibility = View.VISIBLE
            progressBar.visibility = View.GONE
            retrieveList(it)
        })

        viewModel.errorMessage.observe(this,{
            showToast(it)
        })
        viewModel.loading.observe(this,{
            if(it){
                //show progress bar
                progressBar.show()
            }else{
                //hide progressbar
                progressBar.hide()
            }
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutId())
        setupUI()
        viewModel.getUsers2()
    }


    private fun setupUI() {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = MainAdapter(arrayListOf())
        recyclerView.addItemDecoration(
            DividerItemDecoration(
                recyclerView.context,
                (recyclerView.layoutManager as LinearLayoutManager).orientation
            )
        )
        recyclerView.adapter = adapter
    }

    private fun retrieveList(users: List<User>) {
        adapter.apply {
            addUsers(users)
            notifyDataSetChanged()
        }
    }
}