package com.parth.baselauncher.utilities

class Constant {
    companion object {

        const val ACCESS_TOKEN="app_access_token"

        const val USER_ID="userId"

        const val BASE_URL = "base_utl"

        //All OK
        const val STATUS_CODE_200 = 200

        //Authorization error
        const val STATUS_CODE_401 = 401

        //Internal Server Error
        const val STATUS_CODE_500 = 500
    }
}