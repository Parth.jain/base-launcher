package com.parth.baselauncher.utilities

import android.content.Context
import android.util.DisplayMetrics

object Utils {
    fun calculateNoOfColumns(context: Context): Int {
        val displayMetrics: DisplayMetrics = context.resources.displayMetrics
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        // Where 180 is the width of your grid item. You can change it as per your convention.
        return (dpWidth / 180).toInt()
    }
}
