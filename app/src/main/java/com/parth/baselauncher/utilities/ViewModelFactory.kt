package com.parth.baselauncher.utilities

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.parth.baselauncher.retrofit.ApiRepository
import com.parth.baselauncher.viewmodels.RetrofitSampleViewModel
import com.parth.baselauncher.retrofit.ApiHelper
import com.parth.baselauncher.retrofit.ApiService

class ViewModelFactory(private val apiService: ApiService) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        @Suppress("UNCHECKED_CAST")
        if (modelClass.isAssignableFrom(RetrofitSampleViewModel::class.java)) {
            return RetrofitSampleViewModel(ApiRepository(apiService)) as T
        }
        throw IllegalArgumentException("Unknown class name")
    }

}