package com.parth.baselauncher.utilities

import android.content.Context
import android.content.SharedPreferences

object SharedPref {

    private const val NAME = "ToastLauncher"
    private const val MODE = Context.MODE_PRIVATE
    private lateinit var preferences: SharedPreferences

    // list of app specific preferences

    fun init(context: Context) {
        preferences = context.getSharedPreferences(NAME, MODE)
    }

    /**
     * SharedPreferences extension function, so we won't need to call edit() and apply()
     * ourselves on every SharedPreferences operation.
     */
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = edit()
        operation(editor)
        editor.apply()
    }


    fun save(key: String, value: String) {
        preferences.edit { it.putString(key, value).apply() }
    }

    fun getString(key: String, defValue: String): String? {
        return preferences.getString(key, defValue)
    }


    fun clear() {
        preferences.edit { it.clear().apply() }
    }
}