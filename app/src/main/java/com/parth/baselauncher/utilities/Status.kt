package com.parth.baselauncher.utilities


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}