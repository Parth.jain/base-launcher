package com.parth.baselauncher.adapters

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import android.view.ContextMenu
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.parth.baselauncher.R
import com.parth.baselauncher.extensions.openActivity
import com.parth.baselauncher.models.AppData


class AppListAdapter(
    private val context: Context
) : RecyclerView.Adapter<AppListAdapter.AppItemViewHolder>(), View.OnCreateContextMenuListener {

    var appList: List<AppData>? = null
    var filteredList: ArrayList<AppData>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AppItemViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_app, parent, false)
        return AppItemViewHolder(view)
    }

    override fun getItemCount(): Int {
        return appList?.size ?: 0
    }

    override fun onBindViewHolder(holder: AppItemViewHolder, position: Int) {
        holder.appImage.setImageDrawable(appList?.get(position)?.icon)
        holder.appName.text = appList?.get(position)?.appName


        holder.icon_group_full.setOnClickListener {
            it.openActivity(context, appList?.get(position)?.packageName)
        }
    }



    private fun openAppDetail(packageName: String?) {
        try {
            //Open the specific App Info page:
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
            intent.data = Uri.parse("package:$packageName")
            context.startActivity(intent)
        } catch (e: ActivityNotFoundException) {
            //e.printStackTrace();

            //Open the generic Apps page:
            val intent = Intent(Settings.ACTION_MANAGE_APPLICATIONS_SETTINGS)
            context.startActivity(intent)
        }
    }

    fun passAppList(
        appsList: List<AppData>
    ) {
        appList = appsList
        notifyDataSetChanged()
    }

    fun updateList(list: List<AppData>) {
        appList = list
        notifyDataSetChanged()
    }

    inner class AppItemViewHolder(
        view: View
    ) : RecyclerView.ViewHolder(view) {
        val appImage: ImageView = view.findViewById(R.id.appIcon)
        val appName: TextView = view.findViewById(R.id.appName)
        val icon_group_full: ConstraintLayout = view.findViewById(R.id.icon_group_full)
    }

    override fun onCreateContextMenu(
        p0: ContextMenu?,
        p1: View?,
        p2: ContextMenu.ContextMenuInfo?
    ) {
        p0?.add(0, p1?.id!!, 0, "App Info")
    }

}
