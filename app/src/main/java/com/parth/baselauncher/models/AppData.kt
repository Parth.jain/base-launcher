package com.parth.baselauncher.models

import android.graphics.drawable.Drawable

data class AppData (
    val appName:String,
    val icon: Drawable,
    val packageName:String,
)
