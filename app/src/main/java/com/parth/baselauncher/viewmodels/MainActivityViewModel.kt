package com.parth.baselauncher.viewmodels

import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.parth.baselauncher.models.AppData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class MainActivityViewModel : ViewModel() {

    private lateinit var resolvedAppList: List<ResolveInfo>
    var devicesApplicationList = MutableLiveData<ArrayList<AppData>>()

    fun fetchApplicationListFromDevice(packageManager: PackageManager, currentPackageName: String) {
        GlobalScope.launch(Dispatchers.IO) {
            val appList = ArrayList<AppData>()
            resolvedAppList = packageManager
                    .queryIntentActivities(
                            Intent(Intent.ACTION_MAIN, null)
                                    .addCategory(Intent.CATEGORY_LAUNCHER), 0
                    )
            for (ri in resolvedAppList) {
                if (ri.activityInfo.packageName != currentPackageName) {
                    val app = AppData(
                            ri.loadLabel(packageManager).toString(),
                            ri.activityInfo.loadIcon(packageManager),
                            ri.activityInfo.packageName
                    )
                    appList.add(app)
                }
            }
            launch(Dispatchers.Main) {
                devicesApplicationList.value = appList
                // setDockAppForFirst(appList)
                //setAppListInRv(appList)
            }
        }
    }
}
